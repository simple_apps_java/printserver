package com.invariant.devices.posiflex.service.check

import com.invariant.devices.check.CheckHeader

/**
 *
 */
class AuraCheckHeader : CheckHeader {

    private var orderNumber: String? = null
    private var posNumber: String? = null
    private var checkNumber: String? = null
    private var dateTime: String? = null
    private var waiter: String? = null
    private var table: String? = null

    override fun getOrderNumber(): String? {
        return orderNumber
    }

    fun setOrderNumber(value: String) {
        orderNumber = value
    }

    override fun getPosNumber(): String? {
        return posNumber
    }

    fun setPosNumber(value: String) {
        posNumber = value
    }

    override fun getCheckNumber(): String? {
        return checkNumber
    }

    fun setCheckNumber(value: String) {
        checkNumber = value
    }

    override fun getDateTime(): String? {
        return dateTime
    }

    fun setDateTime(value: String) {
        dateTime = value
    }

    override fun getWaiter(): String? {
        return waiter
    }

    fun setWaiter(value: String) {
        waiter = value
    }

    override fun getTable(): String? {
        return table
    }

    fun setTable(value: String) {
        table = value
    }
}