package com.invariant.devices.posiflex.service.check

import com.invariant.devices.check.CheckSummary

/**
 *
 */
class AuraCheckSummary: CheckSummary {

    private var total: String? = null

    override fun getTotal(): String? {
        return total
    }

    fun setTotal(value: String){
        total = value
    }
}