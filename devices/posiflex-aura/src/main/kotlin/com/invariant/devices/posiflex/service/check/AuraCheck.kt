package com.invariant.devices.posiflex.service.check

/**
 *
 */
data class AuraCheck(var header: AuraCheckHeader? = null,
                     val rows: MutableList<AuraCheckRow> = ArrayList(),
                     var summary: AuraCheckSummary? = null){

    fun addRow(row: AuraCheckRow) {
        rows.add(row)
    }
}