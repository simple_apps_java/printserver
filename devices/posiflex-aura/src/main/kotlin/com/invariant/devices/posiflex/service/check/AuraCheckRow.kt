package com.invariant.devices.posiflex.service.check

import com.invariant.devices.check.CheckRow

/**
 *
 */
class AuraCheckRow : CheckRow {

    private var guest:String? = null

    private var goodName:String? = null

    private var count:String? = null

    private var price:String? = null

    override fun getGuest(): String? {
        return guest
    }
    fun setGuest(value: String){
        guest = value
    }

    override fun getGoodName(): String? {
        return goodName
    }

    fun setGoodName(value: String){
        goodName = value
    }

    override fun getCount(): String? {
        return count
    }

    fun setCount(value: String){
        count = value
    }
}