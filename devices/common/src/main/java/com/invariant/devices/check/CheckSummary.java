package com.invariant.devices.check;

public interface CheckSummary {

    String getTotal();

}
