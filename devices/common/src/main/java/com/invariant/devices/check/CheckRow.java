package com.invariant.devices.check;

public interface CheckRow {

    String getGuest();

    String getGoodName();

    String getCount();

}
