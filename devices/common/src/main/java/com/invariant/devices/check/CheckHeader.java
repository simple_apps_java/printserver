package com.invariant.devices.check;

public interface CheckHeader {

    String getOrderNumber();

    String getPosNumber();

    String getCheckNumber();

    String getDateTime();

    String getWaiter();

    String getTable();

}
