package com.invariant.devices.atolfr.data;

public class FrCheckHeader {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
