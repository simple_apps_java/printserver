package com.invariant.saleserver.service

import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.InputStream

@Service
class ResourceService {

    @Autowired
    lateinit var log: Logger

    fun getStream(name: String): InputStream {
        return getResource(name)
    }

    private fun getResource(name: String): InputStream {
        return javaClass.getResourceAsStream(name)
    }

    private fun getFile(name: String): InputStream? {
        var result: InputStream? = null
        try {
            result = FileInputStream(File(name))
        } catch (e: FileNotFoundException) {
            log.error(e.message, e)
        }
        return result
    }
}