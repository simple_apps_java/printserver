package com.invariant.saleserver.service.task

import com.invariant.devices.posiflex.printer.Printer
import java.util.concurrent.Callable

class PrintCheckTask(val checkData: CheckData, val printer: Printer) :
        Callable<PrintCheckResult> {

    var taskId: String? = null

    override fun call(): PrintCheckResult {
        val result = PrintCheckResult()
        printer.print(checkData.check)
        result.status = "success"
        return result
    }


}