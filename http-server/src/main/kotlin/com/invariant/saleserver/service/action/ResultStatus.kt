package com.invariant.saleserver.service.action

enum class ResultStatus {
    SUCCESS,
    ERROR;
}