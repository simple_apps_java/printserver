package com.invariant.saleserver.service

import com.google.gson.Gson
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
class JsonService {

    @Autowired
    lateinit var gson: Gson

    fun toJson(obj: Any): ByteArray = gson.toJson(obj).toByteArray(StandardCharsets.UTF_8)

    fun <T> fromJson(json: String, clazz: Class<T>): T = gson.fromJson(json, clazz)

}