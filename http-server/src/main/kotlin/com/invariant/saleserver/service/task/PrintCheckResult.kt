package com.invariant.saleserver.service.task

class PrintCheckResult(var action: String = "",
                       var status: String = "",
                       var description: String = "")