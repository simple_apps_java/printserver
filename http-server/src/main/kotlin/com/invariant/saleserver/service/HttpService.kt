package com.invariant.saleserver.service

import com.google.common.net.HttpHeaders
import com.sun.net.httpserver.HttpExchange
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.io.IOException
import java.io.InputStream
import java.nio.charset.StandardCharsets
import java.util.*

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
class HttpService {

    @Autowired
    lateinit var log: Logger

    @Throws(IOException::class)
    fun writeJson(httpExchange: HttpExchange, data: ByteArray) {
        addCorsHeaders(httpExchange)
        httpExchange.sendResponseHeaders(200, data.size.toLong())
        httpExchange.responseHeaders.set("Content-Type", "application/json; charset=UTF-8")
        httpExchange.responseBody.write(data)
        httpExchange.responseBody.close()
    }

    @Throws(IOException::class)
    fun writeOptionsResponse(httpExchange: HttpExchange) {
        addCorsHeaders(httpExchange)
        httpExchange.sendResponseHeaders(200, 0)
        httpExchange.responseBody.close()
    }

    @Throws(IOException::class)
    fun addCorsHeaders(httpExchange: HttpExchange) {
        httpExchange.responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
        httpExchange.responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "PROPFIND, PROPPATCH, COPY, MOVE, DELETE," +
                " MKCOL, LOCK, UNLOCK, PUT, GETLIB, VERSION-CONTROL, CHECKIN, CHECKOUT, UNCHECKOUT, REPORT, UPDATE," +
                " CANCELUPLOAD, HEAD, OPTIONS, GET, POST")
        httpExchange.responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, "Overwrite, Destination, Content-Type, "
                + "Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control")
        httpExchange.responseHeaders.set(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "86400")
    }

    @Throws(IOException::class)
    fun writeMethodNotAlowed(httpExchange: HttpExchange, message: ByteArray) {
        httpExchange.sendResponseHeaders(405, 0)
        httpExchange.responseBody.write(message)
        httpExchange.responseBody.close()
    }

    @Throws(IOException::class)
    fun readBody(requestBody: InputStream): String {
        return requestBody.bufferedReader(StandardCharsets.UTF_8).use { it.readText() }
    }

    fun getParameters(httpExchange: HttpExchange): Map<String, String> {
        val parameters = HashMap<String, String>()
        val query = httpExchange.requestURI.query
        if (query != null) {
            for (pair in query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                val nameValue = pair.split("=".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (nameValue.size != 2) {
                    continue
                }
                parameters.put(nameValue[0], nameValue[1])
            }
        }
        return parameters
    }
}