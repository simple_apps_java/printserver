package com.invariant.saleserver.service.task

import com.invariant.devices.posiflex.printer.Printer

data class DeviceStatusResult(var status: String = "",
                              var printer: Printer? = null)