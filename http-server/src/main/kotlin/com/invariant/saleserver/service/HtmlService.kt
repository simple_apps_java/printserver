package com.invariant.saleserver.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.InputStream

@Service
class HtmlService {

    @Autowired
    lateinit var resourceService: ResourceService

    fun getIndex(): InputStream = resourceService.getStream("/view/index.html")

    fun getApi(): InputStream = resourceService.getStream("/view/api.html")

    fun getCheckExample(): InputStream = resourceService.getStream("/view/checkExample.html")

    fun getDeviceSettings(): InputStream = resourceService.getStream("/view/devicesettings.html")

}