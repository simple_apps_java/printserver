package com.invariant.saleserver.service.task

import com.invariant.devices.posiflex.service.check.AuraCheck
import com.invariant.devices.posiflex.service.check.Device

data class CheckData(var device: Device? = null,
                     var check: AuraCheck? = null)