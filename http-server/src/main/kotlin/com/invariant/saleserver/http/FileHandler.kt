package com.invariant.saleserver.http

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import lombok.Cleanup
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import java.io.IOException
import java.io.InputStream

abstract class FileHandler: HttpHandler {

    val BUFFER_SIZE = 1024

    @Autowired
    lateinit var log: Logger

    @Throws(IOException::class)
    override fun handle(httpExchange: HttpExchange) {
        val fileId = httpExchange.requestURI.path
        log.info("Process request file:" + fileId)
        @Cleanup val input = getStream(fileId)
        if (input != null) {
            httpExchange.sendResponseHeaders(200, 0)
            @Cleanup val output = httpExchange.responseBody
            input.copyTo(output, BUFFER_SIZE)
            output.close()
        } else {
            writeError(httpExchange)
        }
    }

    protected abstract fun getStream(fileId: String): InputStream?

    @Throws(IOException::class)
    protected fun writeError(httpExchange: HttpExchange) {
        val response = "Error 404 File not found."
        httpExchange.sendResponseHeaders(404, response.length.toLong())
        @Cleanup val output = httpExchange.responseBody
        output.write(response.toByteArray())
        output.flush()
    }

}