package com.invariant.saleserver.http

import com.invariant.saleserver.service.ResourceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.InputStream

@Component("staticHttpHandler")
class StaticHandler: FileHandler() {

    @Autowired
    private val resourceService: ResourceService? = null

    override fun getStream(fileId: String): InputStream? {
        return resourceService!!.getStream(fileId)
    }

}