package com.invariant.saleserver.http

import com.invariant.devices.posiflex.printer.Printer
import com.invariant.devices.service.serial.SerialPortConfiguration
import com.invariant.saleserver.service.DeviceService
import com.invariant.saleserver.service.JsonService
import com.sun.net.httpserver.HttpExchange
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException
import java.util.stream.Collectors

@Component("devicesListHandler")
class DevicesListHandler : CorsHandler() {

    @Autowired
    lateinit var deviceService: DeviceService

    @Autowired
    lateinit var jsonService: JsonService

    override fun handle(httpExchange: HttpExchange?) {
        val requestMethod = httpExchange?.getRequestMethod()
        if (requestMethod == "GET") {
            doGet(httpExchange)
        } else if (requestMethod == "OPTIONS") {
            doOptions(httpExchange)
        } else {
            writeMethodNotAlowed(httpExchange!!)
        }
    }

    @Throws(IOException::class)
    private fun doGet(httpExchange: HttpExchange) {
        val parameters = httpService.getParameters(httpExchange)
        val printersConfigurations = getDevicesConfigurations()
        httpService.writeJson(httpExchange, jsonService.toJson(printersConfigurations))
    }

    private fun getDevice(id: String): Printer {
        return deviceService.getDevice(id)
    }

    private fun getDevicesConfigurations(): List<SerialPortConfiguration> {
        return deviceService.getDevices().stream().map({ device -> device.getConfiguration() }).collect(Collectors.toList<SerialPortConfiguration>())
    }

}