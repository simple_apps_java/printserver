package com.invariant.saleserver.http

import com.invariant.saleserver.service.HttpService
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import org.springframework.beans.factory.annotation.Autowired
import java.io.IOException

abstract class CorsHandler : HttpHandler {

    @Autowired
    lateinit var httpService: HttpService

    @Throws(IOException::class)
    protected fun doOptions(httpExchange: HttpExchange) {
        httpService!!.writeOptionsResponse(httpExchange)
    }

    @Throws(IOException::class)
    protected fun writeMethodNotAlowed(httpExchange: HttpExchange) {
        val mesage = "405 Method " + httpExchange.requestMethod + " not allowed "
        httpService!!.writeMethodNotAlowed(httpExchange, mesage.toByteArray())
    }
}