package com.invariant.saleserver.http

import com.invariant.saleserver.service.HtmlService
import com.invariant.saleserver.service.HttpService
import com.sun.net.httpserver.HttpExchange
import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException

@Component("deviceSettingsHandler")
@Slf4j
class DeviceSettingsHandler : CorsHandler() {

    @Autowired
    lateinit var htmlService: HtmlService

    override fun handle(httpExchange: HttpExchange?) {
        val requestMethod = httpExchange?.requestMethod
        if (requestMethod == "GET") {
            doGet(httpExchange)
        } else if (requestMethod == "OPTIONS") {
            doOptions(httpExchange)
        } else {
            writeMethodNotAlowed(httpExchange!!)
        }
    }

    @Throws(IOException::class)
    private fun doGet(httpExchange: HttpExchange) {
        httpService.addCorsHeaders(httpExchange);
        httpExchange.sendResponseHeaders(200, 0)
        val outputStream = httpExchange.responseBody
        val inputStream = htmlService.getDeviceSettings()
        inputStream?.copyTo(outputStream)
        inputStream?.close()
        outputStream.close()
    }

}