package com.invariant.saleserver.http

import com.google.gson.Gson
import com.invariant.saleserver.service.HtmlService
import com.invariant.saleserver.service.HttpService
import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.io.IOException

@Component("indexHandler")
class IndexHandler: HttpHandler {

    val BUFFER_SIZE = 1024

    @Autowired
    lateinit var htmlService: HtmlService

    @Autowired
    lateinit var httpService: HttpService

    @Autowired
    lateinit var gson: Gson

    @Throws(IOException::class)
    override fun handle(httpExchange: HttpExchange) {
        httpService!!.addCorsHeaders(httpExchange)
        httpExchange.sendResponseHeaders(200, 0)
        val input = htmlService.getIndex()
        val output = httpExchange.responseBody
        input.copyTo(output, BUFFER_SIZE)
        output.close()
    }
}