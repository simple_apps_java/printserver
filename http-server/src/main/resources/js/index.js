document.addEventListener('DOMContentLoaded', function () {
    var requestData = function (url, ul, onreadystatechange) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.send();
        if(onreadystatechange){
            xhr.onreadystatechange = onreadystatechange;
        }else {
            xhr.onreadystatechange = getOnReadyStateChange(ul);
        }
    }

    var connectedDevices = document.getElementById('connected-devices');
    if (connectedDevices) {
        requestData('/api/devices', connectedDevices, getOnReadyStateChangeEl({el: '#connected-devices'}));
    }

    var serialPorts = document.getElementById('serial-ports');
    if (serialPorts) {
        requestData('/api/serialports', serialPorts, getOnReadyStateChangeEl({el: '#serial-ports'}));
    }

    var checkList = document.getElementById('check-list');
    if (checkList) {
        requestData('/api/checklist', checkList, getOnReadyStateChangeEl({el: '#check-list'}));
    }

});

function getOnReadyStateChange(ul) {
    return function (e) {
        var xhr = e.target;
        if (xhr.readyState != 4) {
            return;
        }
        if (xhr.status != 200) {
            console.error(xhr.status + ': ' + xhr.statusText);
        } else {
            var items = JSON.parse(xhr.responseText);
            items.forEach(function (item) {
                var li = document.createElement('li');
                li.innerHTML = JSON.stringify(item);
                ul.appendChild(li);
            });
            if (items.length == 0) {
                var li = document.createElement('li');
                li.innerHTML = 'Отсутствуют';
                ul.appendChild(li);
            }
        }
    };
}

function getOnReadyStateChangeEl(config){
    return function (e) {
        var xhr = e.target;
        if (xhr.readyState != 4) {
            return;
        }
        if (xhr.status != 200) {
            console.error(xhr.status + ': ' + xhr.statusText);
        } else {
            config.data = {
                items: JSON.parse(xhr.responseText)
            };
            new Vue(config);
        }
    };
}